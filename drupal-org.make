; $Id$

; Drupal Core
core = 6.16

; Display Suite
projects[ds] = 1.1
projects[nd] = 2.1
projects[nd_contrib] = 2.1
projects[cd] = 1.1
projects[ud] = 1.0
projects[vd] = 1.0

; DS integration
projects[nodesinblock] = 1.5
projects[custom_formatters] = 1.5-beta1
projects[tabs] = 1.3

; Token
projects[token] = 1.12

; CCK & Views
projects[cck] = 2.6
projects[filefield] = 3.2
projects[imagefield] = 3.2
projects[views] = 2.10
projects[cck_fieldgroup_tabs] = 1.2

; Panels
projects[panels] = 3.3
projects[ctools] = 1.3

; Apachesolr
projects[apachesolr] = 2.0-beta2

; Voting
projects[votingapi] = 2.3
projects[fivestar] = 1.19

; Gmap & Location
projects[gmap] = 1.1-rc1
projects[location] = 3.1-rc1 

; Ubercart
projects[ubercart] = 2.2

; Various non-display suite related
projects[admin_menu] = 3.0-alpha4
projects[nodeformcols] = 1.6
